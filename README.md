#Roles

#####Table of Contents
1. [Overview](#overview)
2. [Usage - Configuration options and additional functionality](#usage)
3. [Reference - Classes and contributors](#reference)

##Overview
Each node will be assigned a role.

###Module Description
Each role will contain `include <profile-class>`. Include fuction is the only that gets added to roles.

##Usage
```puppet
class role::ahip_webserver inherits role {

  include profile::webserver

}
```

##Reference

###Classes

* ahip_webserver.pp
* ahip_cron.pp
* role.pp - Contains profile::base This is inherited by all other roles.

###Contributors

Mike Travis - <mtravis@webcourseworks.com>