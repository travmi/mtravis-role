class role::ahip_webserver inherits role {

  include profile::ahip::apache
  include profile::ahip::nginx
  include profile::php 
  include profile::ahip::mounts
  include profile::ahip::hosts
  include profile::ahip::firewall
  
}