class role::ahip_cron inherits role {
  
  include profile::ahip::apache
  include profile::ahip::nginx
  include profile::ahip::php 
  include profile::ahip::mounts
  include profile::ahip::hosts
  include profile::ahip::firewall
  
}