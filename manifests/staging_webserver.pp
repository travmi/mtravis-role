class role::staging_webserver inherits role {

  include profile::staging::apache
  include profile::staging::nginx
  include profile::php 
  include profile::staging::mounts
  include profile::staging::firewall
  
}